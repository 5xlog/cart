package com.pentastagiu.utils;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.pentastagiu.enums.Status;
import com.pentastagiu.response.ResponseStatus;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CartServiceResponseTest {

	@Test
	public void shouldReturnStatusOk() {

		List<String> cartServiceResponse = new ArrayList<>();

		List<ResponseStatus> responseStatues = CartServiceResponse.createResponseStatusesList(cartServiceResponse);

		assertThat(responseStatues.isEmpty(), is(false));
		assertThat(responseStatues.get(0).getStatus().equals(Status.OK), is(true));
	}

	@Test
	public void shouldReturnStatusError() {

		List<String> cartServiceResponse = Arrays.asList(CartServiceConstants.ALREADY_EXISTS);

		List<ResponseStatus> responseStatues = CartServiceResponse.createResponseStatusesList(cartServiceResponse);

		assertThat(responseStatues.isEmpty(), is(false));
		assertThat(responseStatues.get(0).getStatus().equals(Status.ERROR), is(true));
	}
}
