package com.pentastagiu.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.pentastagiu.models.CartItem;
import com.pentastagiu.repository.CartItemRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CartItemServiceTest {

	@Mock
	private CartItemRepository cartItemRepository;

	@Test
	public void findByIdShouldReturnACartItemEntity() {
		CartItem cartItem = new CartItem();
		cartItem.setCartItemId(1L);
		when(cartItemRepository.findById(1l)).thenReturn(Optional.of(cartItem));
		assertEquals(cartItem, cartItemRepository.findById(1l).get());
	}

	@Test
	public void findByIdShouldReturnAError() {
		CartItem cartItem = new CartItem();
		cartItem.setCartItemId(2L);
		when(cartItemRepository.findById(1l)).thenReturn(Optional.of(cartItem));
		assertEquals(cartItem, cartItemRepository.findById(1l).get());
	}

}
