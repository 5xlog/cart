package com.pentastagiu.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.pentastagiu.repository.CartRepository;
import com.pentastagiu.utils.CartServiceConstants;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CartServiceTest {

	@Autowired
	CartService cartService;

	@MockBean
	CartRepository cartRepository;

	@Before
	public void setUp() throws Exception {
		Mockito.when(cartRepository.existsByAccountId((long) 2)).thenReturn(true);
	}

	@Test
	public void shouldReturnEmptyList() {

		List<String> cartServiceResponse = cartService.createCart((long) 1);

		assertThat(cartServiceResponse.isEmpty(), is(true));
	}

	@Test
	public void shouldReturnListWithAlreadyExistsMessage() {

		List<String> cartServiceResponse = cartService.createCart((long) 2);

		assertThat(cartServiceResponse.isEmpty(), is(false));
		assertEquals(cartServiceResponse.get(0), CartServiceConstants.ALREADY_EXISTS);
	}

}
