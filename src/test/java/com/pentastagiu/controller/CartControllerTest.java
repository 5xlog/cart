package com.pentastagiu.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.pentastagiu.enums.Status;
import com.pentastagiu.repository.CartItemRepository;
import com.pentastagiu.response.Response;
import com.pentastagiu.response.ResponseStatus;
import com.pentastagiu.service.CartItemService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CartControllerTest {

	//@Autowired
	MockMvc mockMvc;

	@Mock
	CartItemService cartItemService;
	
	@Mock
	CartItemRepository cartItemRepository;

	@InjectMocks
	CartController cartController;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(cartController);
		mockMvc = MockMvcBuilders.standaloneSetup(cartController).build();
	}

	@Test
	public void deleteProdShouldReturnStatusOk() throws Exception {
		Response<String> response = new Response<>();
		ResponseStatus rs = new ResponseStatus();
		rs.setStatus(Status.OK);
		List<ResponseStatus> responseStatuses = Arrays.asList(rs);
		response.setResponseStatuses(responseStatuses);
		when(cartItemService.deleteProduct(1L, 1L)).thenReturn(rs);
		mockMvc.perform(delete("/product").param("cartId", "1").param("itemId", "1")).andExpect(status().isOk());
	}
	
	@Test
	public void deleteProdShouldReturnBadRequest() throws Exception {
		Response<String> response = new Response<>();
		ResponseStatus rs = new ResponseStatus();
		rs.setStatus(Status.ERROR);
		List<ResponseStatus> responseStatuses = Arrays.asList(rs);
		response.setResponseStatuses(responseStatuses);
		when(cartItemService.deleteProduct(1L, 1L)).thenReturn(rs);
		mockMvc.perform(delete("/product").param("cartId", "1").param("itemId", "1")).andExpect(status().isBadRequest());
	}
	

}
