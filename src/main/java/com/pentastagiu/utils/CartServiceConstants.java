package com.pentastagiu.utils;

/**
 * Constants used in the cart service
 * 
 * @author Constantin Listar
 *
 */
public final class CartServiceConstants {

	public static final String ALREADY_EXISTS = "There is already a cart with this ID";
	public static final String DATABASE_ERROR_MESSAGE = "The cart couldn't be saved in the database";
	public static final String CART_CREATION_MESSAGE = "The cart was created successfully";

	private CartServiceConstants() {
	}
}
