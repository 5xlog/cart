package com.pentastagiu.utils;

import java.util.ArrayList;
import java.util.List;

import com.pentastagiu.enums.Status;
import com.pentastagiu.response.ResponseStatus;

/**
 * Creates the response status list used by the controller
 * 
 * @author Constantin Listar
 *
 */
public final class CartServiceResponse {

	private CartServiceResponse() {
	}

	/**
	 * Create the list for the final response
	 * 
	 * @param cartServiceResponse contains the response from the cart service
	 * @return a list of response statuses
	 */
	public static List<ResponseStatus> createResponseStatusesList(List<String> cartServiceResponse) {

		List<ResponseStatus> responseStatuses = new ArrayList<>();
		ResponseStatus responseStatus = new ResponseStatus();

		if (cartServiceResponse.isEmpty()) {
			responseStatus.setStatus(Status.OK);
			responseStatus.setMessage(CartServiceConstants.CART_CREATION_MESSAGE);
		} else {
			responseStatus.setStatus(Status.ERROR);
			responseStatus.setMessage(cartServiceResponse.get(0));
		}

		responseStatuses.add(responseStatus);

		return responseStatuses;
	}

}
