package com.pentastagiu.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pentastagiu.enums.Status;
import com.pentastagiu.models.CartItem;
import com.pentastagiu.repository.CartItemRepository;
import com.pentastagiu.response.ResponseStatus;
import com.pentastagiu.utils.ResponseStatusMessage;

@Service
public class CartItemService {

	@Autowired
	private CartItemRepository cartItemRepository;

	public ResponseStatus deleteProduct(Long cartId, Long itemId) {

		ResponseStatus responseStatus = new ResponseStatus();

		Optional<CartItem> cartItem = cartItemRepository.findById(cartId);
		if (cartItem.isPresent()) {

			if (cartItemRepository.existsByCartItemId(itemId)) {
				cartItemRepository.deleteById(itemId);
				responseStatus.setStatus(Status.OK);
				responseStatus.setMessage(ResponseStatusMessage.SUCCESS_VALIDATION_MESSAGE);
			} else {
				responseStatus.setStatus(Status.ERROR);
				responseStatus.setMessage(ResponseStatusMessage.ERROR_VALIDATION_MESSAGE);
			}
		} else {
			responseStatus.setStatus(Status.ERROR);
			responseStatus.setMessage(ResponseStatusMessage.ERROR_VALIDATION_MESSAGE);
		}

		return responseStatus;

	}

}
