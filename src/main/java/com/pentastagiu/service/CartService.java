package com.pentastagiu.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pentastagiu.models.Cart;
import com.pentastagiu.repository.CartRepository;
import com.pentastagiu.utils.CartServiceConstants;

/**
 * Responsible with the cart operations
 * 
 * @author Constantin Listar
 *
 */
@Service
public class CartService {

	@Autowired
	private CartRepository cartRepository;

	/**
	 * Creates a cart for a single account
	 * 
	 * @param accountId the id of the account that is going to have a cart
	 * @return a list of error messages
	 */
	public List<String> createCart(Long accountId) {

		List<String> errorMessages = new ArrayList<>();

		if (cartRepository.existsByAccountId(accountId)) {
			errorMessages.add(CartServiceConstants.ALREADY_EXISTS);
		} else {

			Cart cart = new Cart(accountId);

			try {
				cartRepository.save(cart);
			} catch (Exception e) {
				errorMessages.add(CartServiceConstants.DATABASE_ERROR_MESSAGE);
			}
		}

		return errorMessages;
	}
	

}
