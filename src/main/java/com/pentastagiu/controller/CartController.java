package com.pentastagiu.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.pentastagiu.enums.Status;
import com.pentastagiu.response.Response;
import com.pentastagiu.response.ResponseStatus;
import com.pentastagiu.service.CartItemService;
import com.pentastagiu.service.CartService;
import com.pentastagiu.utils.CartServiceResponse;

/**
 * Controller for cart
 * 
 * @author Constantin Listar
 *
 */
@EnableWebMvc
@RestController
public class CartController {

	@Autowired
	CartService cartService;

	@Autowired
	CartItemService cartItemService;

	/**
	 * Consumes the body from "/v1/cart" and produces a JSON containing the response
	 * based on the data returned from the cart service
	 * 
	 * @param the ID of the account that has the cart
	 * @return OK, else BAD_REQUEST if there are errors
	 */
	@PostMapping(path = "/v1/cart", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Response<String>> createUniqueCartForAccount(@RequestParam Long accountId) {

		List<String> cartServiceResponse = cartService.createCart(accountId);
		Response<String> response = new Response<>();

		response.setResponseStatuses(CartServiceResponse.createResponseStatusesList(cartServiceResponse));

		if (response.getResponseStatuses().get(0).getStatus().equals(Status.OK)) {
			return ResponseEntity.ok(response);
		} else {

			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		}
	}

	@DeleteMapping(path = "/product")
	public ResponseEntity<Response<String>> deleteProductFromCart(@RequestParam Long cartId,
			@RequestParam Long itemId) {

		Response<String> response = new Response<>();
		List<ResponseStatus> responseStatuses = Arrays.asList(cartItemService.deleteProduct(cartId, itemId));
		System.out.println(cartItemService.deleteProduct(cartId, itemId).getStatus().toString());
		response.setResponseStatuses(responseStatuses);
		System.out.println(response.getResponseStatuses().get(0).getStatus());
		if (response.getResponseStatuses().get(0).getStatus().equals(Status.OK)) {
			return ResponseEntity.ok(response);

		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);

	}

}
