package com.pentastagiu.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.pentastagiu.models.Cart;

/**
 * Operations with the database
 * 
 * @author Constantin Listar
 *
 */
@Repository
public interface CartRepository extends CrudRepository<Cart, Long> {

	boolean existsByAccountId(Long accountId);
	
	Optional<Cart> findById(Long cartId);
	
	boolean existsById(Long id);
	
	
	
}
