package com.pentastagiu.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.pentastagiu.models.CartItem;

@Repository
public interface CartItemRepository extends CrudRepository<CartItem, Long> {
	public boolean existsByCartItemId(Long cartItem);

	public Optional<CartItem> findById(Long cartId);

	public void deleteById(Long itemId);

}
